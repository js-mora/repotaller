var clientesObtenidos;
function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}
function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  //alert(JSONClientes.value[0].ProductName);
  var divTablaClientes = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  //tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONClientes.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var imgBandera = document.createElement("img");
    var columnaCompanyName = document.createElement("td");
    var columnaCity = document.createElement("td");
    var columnaCountry = document.createElement("td");
    var columnaBandera = document.createElement("td");
    imgBandera.classList.add("flag");
    if (JSONClientes.value[i].Country == "UK") {
     imgBandera.src = rutaBandera + "United-Kingdom.png";
    } else {
     imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    columnaCompanyName.innerText = JSONClientes.value[i].CompanyName;
    columnaCity.innerText = JSONClientes.value[i].City;
    //columnaCountry.innerText = JSONClientes.value[i].Country;
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaCompanyName);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaBandera);
    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
